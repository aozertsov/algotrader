//package net.ozertsov.algotrading.predict.ml
//
//import java.time.Instant
//
//case class PredCandle(
//  from: Long,
//  to: Long,
//  `type`: String,
//  openPrice: String,
//  closePrice: String,
//  lowPrice: String,
//  highPrice: String,
//  volume: String,
//  openPredictPrice: String
//)
//{
//  def apply(
//             from: Instant,
//             to: Instant,
//             `type`: String,
//             openPrice: String,
//             closePrice: String,
//             lowPrice: String,
//             highPrice: String,
//             volume: String,
//             openPredictPrice: String
//           ): PredCandle = new PredCandle(from.getEpochSecond, to.getEpochSecond, `type`, openPrice, closePrice, lowPrice, highPrice, volume, openPredictPrice)
//}
