package net.ozertsov.algotrading.crowl;

import net.ozertsov.algotrading.model.HistoryKeeper;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Trades;
import org.knowm.xchange.exmo.service.ExmoMarketDataService;
import org.knowm.xchange.exmo.service.ExmoTradeService;

public class ExmoCrowler extends AbstractCrowler<ExmoMarketDataService, ExmoTradeService, Trades> {
    public ExmoCrowler(CurrencyPair tradingPair, Class exchangeClass, HistoryKeeper keeper) {
        super(tradingPair, exchangeClass, keeper);
    }
}
