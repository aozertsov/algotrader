package net.ozertsov.algotrading.io;

import com.google.common.io.Resources;
import net.ozertsov.algotrading.analyze.ChunkBasedAnalyzer;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class HistoryReaderTest {
    @Test
    public void readerNotCrushed() throws URISyntaxException {
        URL uri = Resources.getResource("crowlerTestDataSet.csv");
        HistoryReader reader = new HistoryReader(new File(uri.toURI()));
        List<JapanesseCandlestick> candlestickList = reader.readHistory();

        System.out.println(candlestickList.size());
        System.out.println(candlestickList.get(424).getVolume());

        assertEquals(candlestickList.size(), 436);


        ChunkBasedAnalyzer analyzer = new ChunkBasedAnalyzer();
        System.out.print("Resistance line =");
        System.out.println(analyzer.getResistancePrice(candlestickList.subList(candlestickList.size() - 11, candlestickList.size() - 1)));
        System.out.print("Support line =");
        System.out.println(analyzer.getSupportPrice(candlestickList.subList(candlestickList.size() - 11, candlestickList.size() - 1)));

    }
}
