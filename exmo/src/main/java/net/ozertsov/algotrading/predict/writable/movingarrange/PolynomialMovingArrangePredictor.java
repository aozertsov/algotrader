package net.ozertsov.algotrading.predict.writable.movingarrange;

import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.model.candlestick.CandlestickType;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import net.ozertsov.algotrading.predict.evaluate.EvaluationException;
import net.ozertsov.algotrading.predict.evaluate.PolynomialMovingArrangePredictEvaluator;
import net.ozertsov.algotrading.predict.writable.MovingArrangePredictor;

import java.time.Instant;
import java.util.List;

@Log4j2
public class PolynomialMovingArrangePredictor extends MovingArrangePredictor {


    public PolynomialMovingArrangePredictor(String folder, String suffix, int degree, double[] coeffs) {
        this(folder, "polynomial_predict", suffix, degree, coeffs);
    }

    public PolynomialMovingArrangePredictor(String folder, String prefix, String suffix, int degree, double[] coeffs) {
        super(degree, folder, prefix, suffix);
        if (coeffs.length != degree) {
            throw new IllegalArgumentException("coefficients count not equals degree of polynom");
        }
        this.evaluator = new PolynomialMovingArrangePredictEvaluator(coeffs);
    }

    @Override
    public JapanesseCandlestick predictCandle(List<JapanesseCandlestick> candles) {
        try {
            return evaluator.predictCandle(candles);
        } catch (EvaluationException e) {
            log.error(e);
        }
        return null;
    }

    @Override
    protected boolean isShouldBeExcluded(JapanesseCandlestick candle) {
        return candle.getType() == CandlestickType.EMPTY && candle.getOpenPrice() == 0;
    }
}
