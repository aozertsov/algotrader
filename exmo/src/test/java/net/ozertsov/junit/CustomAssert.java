package net.ozertsov.junit;

import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import org.junit.Assert;

public class CustomAssert {
    protected CustomAssert() {
    }

    static public void assertEquals(JapanesseCandlestick expected, JapanesseCandlestick actual) {
        assertEquals(null, expected, actual);
    }

    static public void assertEquals(String message, JapanesseCandlestick expected,
                                    JapanesseCandlestick actual) {
        Assert.assertEquals(message, expected.getFrom(), actual.getFrom());
        Assert.assertEquals(message, expected.getTo(), actual.getTo());
        Assert.assertEquals(message, expected.getType(), actual.getType());
        Assert.assertEquals(message, expected.getOpenPrice(), actual.getOpenPrice(), 0.1e-10);
        Assert.assertEquals(message, expected.getClosePrice(), actual.getClosePrice(), 0.1e-10);
        Assert.assertEquals(message, expected.getLowPrice(), actual.getLowPrice(), 0.1e-10);
        Assert.assertEquals(message, expected.getHighPrice(), actual.getHighPrice(), 0.1e-10);
        Assert.assertEquals(message, expected.getVolume(), actual.getVolume(), 0.1e-10);
    }
}
