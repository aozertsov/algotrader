//package net.ozertsov.algotrading.predict.ml;
//
//import lombok.Data;
//
//import java.time.Instant;
//
//@Data
//public class PredictionCandle {
//    long from;
//    long to;
//    String type;
//    double openPrice;
//    double closePrice;
//    double lowPrice;
//    double highPrice;
//    double volume;
//    double openPredictPrice;
//
//    public PredictionCandle(Instant from, Instant to, String type, double openPrice, double closePrice, double lowPrice, double highPrice, double volume, double openPredictPrice) {
//        this.from = from.getEpochSecond();
//        this.to = to.getEpochSecond();
//        this.type = type;
//        this.openPrice = openPrice;
//        this.closePrice = closePrice;
//        this.lowPrice = lowPrice;
//        this.highPrice = highPrice;
//        this.volume = volume;
//        this.openPredictPrice = openPredictPrice;
//    }
//
//    public PredictionCandle(String from, String to, String type, double openPrice, double closePrice, double lowPrice, double highPrice, double volume, double openPredictPrice) {
//        this(Instant.parse(from), Instant.parse(to), type, openPrice, closePrice, lowPrice, highPrice, volume, openPredictPrice);
//    }
//}
