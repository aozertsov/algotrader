package net.ozertsov.algotrading.trade;

import lombok.Getter;
import lombok.ToString;
import org.knowm.xchange.dto.trade.LimitOrder;

import java.time.Instant;

@ToString
public class HistoricalOrder extends LimitOrder {
    @Getter
    Instant until;

    public HistoricalOrder(LimitOrder order, Instant until) {
        super(order.getType(), order.getOriginalAmount(), order.getCurrencyPair(), order.getId(), order.getTimestamp(), order.getLimitPrice());
        this.until = until;
    }
}
