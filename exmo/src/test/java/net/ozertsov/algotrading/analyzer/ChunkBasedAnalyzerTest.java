package net.ozertsov.algotrading.analyzer;

import com.google.common.io.Resources;
import net.ozertsov.algotrading.analyze.ChunkBasedAnalyzer;
import net.ozertsov.algotrading.io.HistoryReader;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

public class ChunkBasedAnalyzerTest {

    @Test
    public void analyzerNotCrushed() throws URISyntaxException {
        URL uri = Resources.getResource("crowlerTestDataSet.csv");
        HistoryReader reader = new HistoryReader(new File(uri.toURI()));
        List<JapanesseCandlestick> candlestickList = reader.readHistory();

        ChunkBasedAnalyzer analyzer = new ChunkBasedAnalyzer();
        System.out.print("Resistance line =");
        System.out.println(analyzer.getResistancePrice(candlestickList.subList(candlestickList.size() - 10, candlestickList.size() - 1)));
        System.out.print("Support line =");
        System.out.println(analyzer.getSupportPrice(candlestickList.subList(candlestickList.size() - 10, candlestickList.size() - 1)));


    }
}
