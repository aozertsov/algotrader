package net.ozertsov.algotrading.predict.writable;

import net.ozertsov.algotrading.io.HistoryWriter;
import net.ozertsov.algotrading.predict.Predictor;
import net.ozertsov.algotrading.predict.evaluate.PredictEvaluator;

public abstract class WritablePredictor extends Predictor {

    protected HistoryWriter writer;

    public WritablePredictor(PredictEvaluator evaluator, HistoryWriter writer) {
        super(evaluator);
        this.writer = writer;
    }
}
