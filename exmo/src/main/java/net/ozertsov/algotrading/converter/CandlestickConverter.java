package net.ozertsov.algotrading.converter;

import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import org.knowm.xchange.dto.marketdata.Trade;
import org.knowm.xchange.dto.marketdata.Trades;
import org.knowm.xchange.dto.trade.LimitOrder;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Log4j2
public class CandlestickConverter<TLimitOrder extends LimitOrder, TTrades extends Trades> {
    private double lastPrice;

    public JapanesseCandlestick convert(List<LimitOrder> orders, TTrades trades, long fromSeconds, long toSeconds) {
        List<Trade> filteredTrades = trades.getTrades().stream()
                .filter(x -> x.getTimestamp().getTime() / 1000 >= fromSeconds)
                .filter(x -> x.getTimestamp().getTime() / 1000 < toSeconds)
                .collect(Collectors.toList());
        List<Double> tradesForCandle = filteredTrades.stream()
                .sorted((x, y) -> {
                    long xtime = x.getTimestamp().getTime();
                    long ytime = y.getTimestamp().getTime();
                    if (xtime < ytime) {
                        return -1;
                    }
                    if (xtime > ytime)
                        return 1;
                    return 0;
                })
                .map(Trade::getPrice)
                .map(x -> x.doubleValue())
                .collect(Collectors.toList());


        List<Double> ordersForCandle = orders.stream()
                .map(x -> x.getLimitPrice())
                .map(x -> x.doubleValue())
                .sorted()
                .collect(Collectors.toList());

        double volume = filteredTrades.stream()
                .map(x -> x.getOriginalAmount())
                .reduce(BigDecimal.ZERO, (x, y) -> x.add(y))
                .doubleValue();


        if (tradesForCandle.size() > 0) {
            lastPrice = tradesForCandle.get(tradesForCandle.size() - 1);
            return new JapanesseCandlestick(Instant.ofEpochSecond(fromSeconds), Instant.ofEpochSecond(toSeconds),
                    tradesForCandle.get(0), tradesForCandle.get(tradesForCandle.size() - 1), //TODO может быть, что не было trades в этом моменте
                    ordersForCandle.get(0), ordersForCandle.get(ordersForCandle.size() - 1),
                    volume);
        }
        return new JapanesseCandlestick(Instant.ofEpochSecond(fromSeconds), Instant.ofEpochSecond(toSeconds), lastPrice,
                ordersForCandle.get(0), ordersForCandle.get(ordersForCandle.size() - 1), volume);

    }
}
