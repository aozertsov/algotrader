package net.ozertsov.algotrading.model.history;

import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.model.HistoryKeeper;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;

import java.io.File;

@Log4j2
public class CandleKeeper extends HistoryKeeper<JapanesseCandlestick> {

    public CandleKeeper(int keepInMemory, int bufferSize, File file) {
        super(keepInMemory, bufferSize, file);
    }

    @Override
    public void addElement(JapanesseCandlestick candle) {
        synchronized (currentUnbuffered) {
            if (currentUnbuffered.get() == bufferSize) {
                log.info("try to append list");
                writer.appendData(data.subList(0, bufferSize - keepInMemory));
                data = data.subList(bufferSize - keepInMemory, data.size());
                currentUnbuffered.getAndAdd(-1 * (bufferSize - keepInMemory));
            }
            data.add(candle);
            currentUnbuffered.incrementAndGet();
        }

    }


}
