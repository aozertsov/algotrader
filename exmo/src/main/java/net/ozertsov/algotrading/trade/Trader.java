package net.ozertsov.algotrading.trade;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.io.HistoryWriter;
import net.ozertsov.algotrading.io.Recordable;
import net.ozertsov.algotrading.io.Schema;
import net.ozertsov.algotrading.model.candlestick.CandlestickType;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order;
import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.exmo.dto.trade.ExmoTradeHistoryParams;
import org.knowm.xchange.service.trade.TradeService;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static net.ozertsov.algotrading.io.Schema.Type.ENUM;
import static net.ozertsov.algotrading.io.Schema.Type.INSTANT;
import static net.ozertsov.algotrading.model.candlestick.CandlestickType.LONG;
import static net.ozertsov.algotrading.util.FeeEvaluator.calculateFee;
import static net.ozertsov.algotrading.util.JapanesseCandlestickUtils.findMax;
import static net.ozertsov.algotrading.util.JapanesseCandlestickUtils.findMin;
import static org.knowm.xchange.dto.Order.OrderType.ASK;
import static org.knowm.xchange.dto.Order.OrderType.BID;

//TODO обработать ситуации, когда сильно лажаем

/**
 * Класс, который осуществляет торговлю по определенному инструменту на определенной бирже.
 */
@Log4j2
public class Trader {
    HistoryWriter<TradeOrder> writer;

    double myAvgPrice; //средняя цена моего портфеля
    final double minimalProfitChange; //Минимальное изменение цены, при котором стоит продавать
    final double maxLossChange; // максимальный возможный убыток

    volatile double firstCurrencyAmount; //количество монеты в левой части пары
    volatile double secondCurrencyAmount; //количество монеты в правой части пары
    volatile double currentPrice; //актуальная цена рынка
    CurrencyPair pair; //валютная пара
    TradeHistoryKeeper tradeHistory;
    TradeService traderClient; // client to execute exchange

    volatile double lastAskPrice; //последняя цена, за которую продали
    volatile double lastBidPrice; //последняя цена, за которую купили

    public Trader(Class exchangeClass, CurrencyPair pair, double myAvgPrice, double minimalProfitChange,
                  double firstCurrencyAmount, double secondCurrencyAmount,
                  double lastAskPrice, double lastBidPrice, File tradeFile) {
        this.pair = pair;
        this.myAvgPrice = myAvgPrice;
        this.minimalProfitChange = minimalProfitChange;
        this.maxLossChange = minimalProfitChange / 3.0;
        this.firstCurrencyAmount = firstCurrencyAmount;
        this.secondCurrencyAmount = secondCurrencyAmount;
        this.lastBidPrice = lastBidPrice;
        this.lastAskPrice = lastAskPrice;
        tradeHistory = new TradeHistoryKeeper();
        ExchangeSpecification specification = new ExchangeSpecification(exchangeClass);

        specification.setApiKey("K-4a01c6acb55c9183afcc3186a456804f3ad39353");
        specification.setSecretKey("S-f812621cb14efce1f1e5bf2acf7f822b3590b1d4");
        Exchange ex = ExchangeFactory.INSTANCE.createExchange(specification);
        traderClient = ex.getTradeService();
        writer = HistoryWriter.<TradeOrder>createDocument(tradeFile);
        log.info("Create trader with minimalProfit = {}", minimalProfitChange);
    }

    /**
     * Метод, который решает торговать или нет на основе предиктов
     * @param predictedCandles предсказанные свечи (может быть несколько, зависит от горизонта планирования предиктора)
     * @param momentCandle последняя свеча
     */
    public void trade(List<JapanesseCandlestick> predictedCandles, JapanesseCandlestick momentCandle) {
        try {
            log.info(() -> "Try to trade");
            double possibleCurrentPrice = momentCandle.getClosePrice();
            if (possibleCurrentPrice != 0) {
                currentPrice = possibleCurrentPrice;
            }
            updateHistory(currentPrice);
            Pair<JapanesseCandlestick, JapanesseCandlestick> potentialPrices = computePredictedPriceStats(predictedCandles);

            JapanesseCandlestick candle = potentialPrices.getLeft().getFrom()
                    .isBefore(potentialPrices.getRight().getFrom()) ? potentialPrices.getLeft() : potentialPrices.getRight();
            log.info("potential Prices: {}", potentialPrices);
            log.info("predictedCandle: : {}", candle);
            double potentialPrice = computePrice(candle);
            log.info("Current price = {}", currentPrice);
            log.info("PotentialPrice = {}", potentialPrice);
            log.info("Amount ETH = {}, USD = {}", firstCurrencyAmount, secondCurrencyAmount);

//            if (currentPrice > 0 && potentialPrice - currentPrice > maxLossChange) { //если вдруг предикт говорит, что будет рост
//                System.out.println(secondCurrencyAmount / currentPrice);
//
//                if (secondCurrencyAmount > 0 && lastAskPrice - potentialPrice < 0 && potentialPrice - lastAskPrice > minimalProfitChange) {
//                    //пытаемся купить валюту по актуальной цене (чтобы потом продать дороже)
//                    sendOrder(BID, secondCurrencyAmount / currentPrice, currentPrice, candle.getTo());
//                    secondCurrencyAmount = 0;
//                }
//                if (firstCurrencyAmount > 0 && lastBidPrice - potentialPrice < 0 && potentialPrice - lastBidPrice > minimalProfitChange) {
//                    //выставляем ордер на продажу по предикту, чтобы через время он сработал
//                    sendOrder(ASK, firstCurrencyAmount, potentialPrice, candle.getTo());
//                    firstCurrencyAmount = 0;
//                }
//            }
//            if (currentPrice > 0 && currentPrice - potentialPrice > 0 && currentPrice - potentialPrice > maxLossChange) { //если будет уменьшение цены
//                if (firstCurrencyAmount > 0 && lastBidPrice - potentialPrice < 0 && potentialPrice - lastBidPrice > minimalProfitChange) {
//                    //продем по рынку (чтобы потом закупить дешевле)
//                    sendOrder(ASK, firstCurrencyAmount, currentPrice, candle.getTo());
//                    firstCurrencyAmount = 0;
//                }
//                if (secondCurrencyAmount > 0 && lastAskPrice - potentialPrice < 0 && potentialPrice - lastAskPrice > minimalProfitChange) {
//                    //выставлем ордер на закупку по предикту
//                    sendOrder(BID, secondCurrencyAmount / potentialPrice, potentialPrice, candle.getTo());
//                    secondCurrencyAmount = 0;
//                }
//            }

            //если есть первая валюта
            if (firstCurrencyAmount > 0) {
                //если предикт говорит, что будет рост, который удачно растет
                if (potentialPrice > currentPrice  && potentialPrice - currentPrice > 0 && potentialPrice - currentPrice > minimalProfitChange) {
                    //выставляем ордер на предсказанную цену
                    sendOrder(ASK, firstCurrencyAmount, potentialPrice, candle.getTo());
                }
                //если предикт говорит, что будет падение, которое больше чем наш ожидаемый убыток
//                if (currentPrice > potentialPrice && potentialPrice < lastBidPrice && lastBidPrice - potentialPrice > maxLossChange) {
                if ((potentialPrice < currentPrice && currentPrice > lastBidPrice && currentPrice - lastBidPrice > maxLossChange) ||
                    (currentPrice < lastBidPrice && lastBidPrice - currentPrice > maxLossChange) ||
                    (potentialPrice < currentPrice && potentialPrice < lastBidPrice && lastBidPrice - potentialPrice > maxLossChange)) {
                    //продем по рынку (чтобы потом закупить дешевле)
                    sendOrder(ASK, firstCurrencyAmount, currentPrice, candle.getTo());
                }
            }

            //если есть вторая валюта
            if (secondCurrencyAmount > 0) {
                //если предикт говорит, что будет рост, который удачно растет
//                if (currentPrice < potentialPrice && potentialPrice > lastAskPrice && potentialPrice - lastAskPrice > minimalProfitChange) {
                if (potentialPrice > currentPrice  && potentialPrice - currentPrice > 0 && potentialPrice - currentPrice > minimalProfitChange) {
                    //пытаемся купить валюту по актуальной цене (чтобы потом продать дороже)
                    sendOrder(BID, secondCurrencyAmount / currentPrice, currentPrice, candle.getTo());
                }
                //если предикт говорит, что будет падение, которое больше чем наш ожидаемый убыток
                if (firstCurrencyAmount > 0 && ((potentialPrice < currentPrice && currentPrice > lastBidPrice && currentPrice - lastBidPrice > maxLossChange) ||
                    (currentPrice < lastBidPrice && lastBidPrice - currentPrice > maxLossChange) ||
                    (potentialPrice < currentPrice && potentialPrice < lastBidPrice && lastBidPrice - potentialPrice > maxLossChange))) {
                    //продем по рынку (чтобы потом закупить дешевле)
                    sendOrder(ASK, firstCurrencyAmount, currentPrice, candle.getTo());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, который считает оптимальную цену инструмента, зная его свечу
     * @param candle - японская свеча
     * @return
     */
    protected double computePrice(JapanesseCandlestick candle) {
        if (candle.getType() == LONG) {
            return (0.45 * candle.getOpenPrice() + 0.55 * candle.getClosePrice());
        } else if (candle.getType() == CandlestickType.SHORT) {
            return (0.45 * candle.getClosePrice() + 0.55 * candle.getOpenPrice());
        }
        return (0.5 * candle.getClosePrice() + 0.5 * candle.getOpenPrice());
    }

    /**
     * Метод, который обновляет историю торговли ({@link TradeHistoryKeeper}).
     * 1) удаляет выполненные ордера (с обновлением балансов)
     * 2) удаляет неактуальные ордера
     */
    private void updateHistory(double currentPrice) {
        try {

            ExmoTradeHistoryParams a = new ExmoTradeHistoryParams();
            a.setCurrencyPairs(Collections.singleton(pair));
            //берем все сделки
            List<UserTrade> trades = traderClient.getTradeHistory(a).getUserTrades();

            log.info("try to update history: {} with trades {}", tradeHistory, trades);
            //пересчитываем балансы для выполненных ордеров
            //в истории остались только невыполненные ордера
            for (String id : tradeHistory.getIds()) {
                calculateEnded(id, trades);
            }

            //удаляем неактуальные заказы
            deleteBadOrders(tradeHistory.getTrades(), currentPrice);

//            for (Order o : openOrders) {
//                if (tradeHistory.contains(o)) {
//                    int index = tradeHistory.indexOf(o);
//                    Order realOrder = tradeHistory.get(index);
//                    //обновляем количество валюты
//                    calculateEnded(realOrder.getType(), realOrder.getCumulativeAmount(), o.getCumulativeAmount(),
//                            realOrder.getFee(), o.getFee());
//                    realOrder.setCumulativeAmount(o.getCumulativeAmount());
//                    realOrder.setFee(o.getCumulativeAmount());
//                }
//            }

            //удаляем те ордера, которые не сыграли
//            Instant currentDate = Instant.now();
//            List<HistoricalOrder> needDelete = new ArrayList<>();
//            for (HistoricalOrder order : tradeHistory) {
//                if (order.getUntil().isBefore(currentDate)) {
//                    traderClient.cancelOrder(order.getId());
//                    needDelete.add(order);
//                }
//            }
//            tradeHistory.removeAll(needDelete);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Метод, который формирует на бирже ордер. После установки ордера записывает данные в историю.
     * @param type - тип ордера
     * @param amount - количество инстремента
     * @param price - цена
     * @param untilDate - дата, после которой инструмент должен быть снят с биржи, если невыполнен.
     */
    private void sendOrder(Order.OrderType type, double amount, double price, Instant untilDate) {
        TradeOrder to = new TradeOrder(type, amount, price, Instant.now(), untilDate);
        writer.appendData(to);
        log.info("Try to add order \n type {}, amount: {}, price: {}, instant: {}", type, amount, price, untilDate);
//        try {
//            LimitOrder order = new LimitOrder.Builder(type, pair)
//                    .originalAmount(BigDecimal.valueOf(amount))
//                    .limitPrice(BigDecimal.valueOf(price))
//                    .build();
//            String id = traderClient.placeLimitOrder(order);
//            //если мы покупаем, то нужно заблокировать вторую валюту
//            if (type == ASK) {
//                firstCurrencyAmount -= amount;
//            }
//            if (type == BID) {
//                secondCurrencyAmount -= amount * price;
//            }
//            tradeHistory.add(id, new HistoricalOrder(order, untilDate));
//        } catch (Exception e) {
//            e.printStackTrace();
//            StackTraceElement element = e.getStackTrace()[0];
//            System.err.println(element.getMethodName() + element.getLineNumber());
//        }
    }

    /**
     * Метод, который обновляет данные по сделке
     * @param orderId
     */
    public void calculateEnded(String orderId, List<UserTrade> userTrades) {
        List<UserTrade> trades = userTrades
                .stream()
                .filter(x -> x.getOrderId().equals(orderId))
                .filter(x -> x.getOriginalAmount() != null && !x.getOriginalAmount().equals(BigDecimal.ZERO))
                .collect(Collectors.toList());
        for (UserTrade trade : trades) {
            BigDecimal fee = calculateFee(trade.getOriginalAmount(), trade.getType(),
                    trade.getPrice(), BigDecimal.valueOf(0.002));
            calculateBalance(trade.getType(), trade.getOriginalAmount(), fee, trade.getPrice());
        }
        tradeHistory.removeOrder(orderId);
    }

    private void calculateBalance(Order.OrderType type, BigDecimal amount, BigDecimal fee, BigDecimal price) {
        log.info("calculateBalance");
        //Если продавали, значит надо добавить значение во вторую валюту, за иключением коммиссии
        BigDecimal realFee = fee == null ? BigDecimal.ZERO : fee;
        if (type == ASK) {
            System.out.println(amount);
            System.out.println(realFee);
            secondCurrencyAmount += amount.multiply(price).subtract(realFee).doubleValue();
            lastAskPrice = price.doubleValue();
        }
        //Если покупали = значит надо добавить значение во вторую валюту, за исключением коммиисии
        if (type == BID) {
            System.out.println(amount);
            System.out.println(realFee);
            firstCurrencyAmount += amount.subtract(realFee).doubleValue();
            lastBidPrice = price.doubleValue();
        }
    }

    private void calculateCancel(HistoricalOrder o) {
        System.out.println(o);
        //Если продавали, то сперва возвращаем непроданное количество в кассу
        //А потом пересчитываем, сколько перетекло
        if (o.getType() == ASK) {
            BigDecimal cumulative = o.getCumulativeAmount() == null ? BigDecimal.ZERO : o.getCumulativeAmount();
            firstCurrencyAmount += o.getOriginalAmount().subtract(cumulative).doubleValue();
            calculateBalance(o.getType(), cumulative, BigDecimal.valueOf(0.002), o.getLimitPrice());
        }
        //Если покупали, то сперва возвращаем непроданное количество в кассу
        //А потом пересчитываем сколько перетекло
        else {
            BigDecimal cumulative = o.getCumulativeAmount() == null ? BigDecimal.ZERO : o.getCumulativeAmount();
            secondCurrencyAmount += o.getOriginalAmount().subtract(cumulative).doubleValue();
            calculateBalance(o.getType(), cumulative, BigDecimal.valueOf(0.002), o.getLimitPrice());
        }
    }

    /**
     * Удаляет ордера, которые не попали
     * @param orders
     */
    private void deleteBadOrders(Set<Map.Entry<String, HistoricalOrder>> orders, double currentPrice) {
        Instant currenttTime = Instant.now();
        for (Map.Entry<String, HistoricalOrder> o : orders) {
            try {
                HistoricalOrder order = o.getValue();
                if (order.getUntil().isBefore(currenttTime) ||
                        (order.getType() == ASK && currentPrice > order.getLimitPrice().doubleValue()) ||
                        (order.getType() == BID && currentPrice < order.getLimitPrice().doubleValue()))
                    //TODO 
                traderClient.cancelOrder(o.getKey());
            } catch (IOException e) {
                e.printStackTrace();
            }

            //после отмены удаляем ордер из истории
            //и пересчитываем количество валюты
            tradeHistory.removeOrder(o.getKey());
            calculateCancel(o.getValue());
        }
    }

    protected Pair<JapanesseCandlestick, JapanesseCandlestick> computePredictedPriceStats(List<JapanesseCandlestick> predictedCandles) {
        return new ImmutablePair<>(findMin(predictedCandles), findMax(predictedCandles));
    }


    @AllArgsConstructor
    static
    class TradeOrder implements Recordable {
        org.knowm.xchange.dto.Order.OrderType type;
        double amount;
        double price;
        Instant from;
        Instant until;

        @Override
        public Schema getSchema() {
            return new Schema(Arrays.asList(
                    new Schema.Field("type", INSTANT),
                    new Schema.Field("amount", INSTANT),
                    new Schema.Field("price", ENUM),
                    new Schema.Field("from", INSTANT),
                    new Schema.Field("until", INSTANT)
            ));
        }

        @Override
        public String getData() {
            return String.format("%s,%s,%s,%s", this.type, this.amount, this.price, this.from, this.until);
        }
    }

}