package net.ozertsov.algotrading.predict.evaluate;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;

import java.time.Instant;
import java.util.List;

@Log4j2
@AllArgsConstructor
public class PolynomialMovingArrangePredictEvaluator extends PredictEvaluator {

    double[] coefficients;

    @Override
    public JapanesseCandlestick predictCandle(List<JapanesseCandlestick> candles) throws EvaluationException {
        double openPrice = 0;
        double closePrice = 0;
        double lowPrice = 0;
        double highPrice = 0;
        double volume = 0;
        log.info("try to Predict candles with candles: {}", candles);
        int coef = coefficients.length - 1;
        for (int i = candles.size() - 1; i >= 0 && coef > -1; i--, coef--) {
            openPrice += candles.get(i).getOpenPrice() * coefficients[coef];
            closePrice += candles.get(i).getClosePrice() * coefficients[coef];
            lowPrice += candles.get(i).getLowPrice() * coefficients[coef];
            highPrice += candles.get(i).getHighPrice() * coefficients[coef];
            volume += candles.get(i).getVolume() * coefficients[coef];
        }
        Instant from = candles.get(candles.size() - 1).getTo();
        Instant to = candles.get(candles.size() - 1).getTo()
                .plusSeconds(candles.get(candles.size() - 1).getTo().getEpochSecond() -
                        candles.get(candles.size() - 1).getFrom().getEpochSecond());
        JapanesseCandlestick predictedCandle = new JapanesseCandlestick(from, to, openPrice, closePrice, lowPrice, highPrice, volume);
        return predictedCandle;
    }
}
