package net.ozertsov.algotrading.trade;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.exmo.ExmoExchange;
import org.knowm.xchange.exmo.dto.trade.ExmoTradeHistoryParams;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.knowm.xchange.currency.CurrencyPair.ETH_USD;

@Ignore
public class TraderTest {

    @Rule
    TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void checkEnded() throws IOException {
        Trader trader = new Trader(ExmoExchange.class, ETH_USD, 0, 0.3, 0,
                10, 146, 0, folder.newFolder());

        ExmoTradeHistoryParams a = new ExmoTradeHistoryParams();
        a.setCurrencyPairs(Collections.singleton(ETH_USD));

        List<UserTrade> trades = trader.traderClient.getTradeHistory(a).getUserTrades();

        System.out.println(trades);
        trader.calculateEnded("1883489198", trades);
    }
}
