package net.ozertsov.algotrading.predictor;

import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import net.ozertsov.algotrading.predict.Predictor;
import net.ozertsov.algotrading.predict.writable.MovingArrangePredictor;
import org.junit.Ignore;
import org.junit.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Ignore
public class MovingArrangePredictorTest {

    @Test
    public void testEmptyCandles() {
        List<JapanesseCandlestick> candles = new ArrayList<>();

        JapanesseCandlestick candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"), 119.50999,119.50999,114.65,126.49567876,0.08171997);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"), 119.50999,119.50999,114.65,126.49567876,0.08171997);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"), 119.50999,119.50999,114.65,126.49567876,0.08171997);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"), 0.0,0.0,114.69,126.55253002,0.0);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"), 119.00061502,119.00061502,114.69,126.5,0.1);
        candles.add(candle);

        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"), 119.50999,119.50999,114.65,126.49567876,0.08171997);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:49:55Z"), Instant.parse("2018-12-02T19:50:55Z"), 0.0,0.0,114.69,126.55253002,0.0);
        candles.add(candle);


        Predictor predictor = new MovingArrangePredictor("/media/alexander/Новый том/Repositore/Java/Algotrading");
        System.out.println(predictor.nextCandle(candles));
    }

    @Test
    public void testEmptyCandles1() {
        List<JapanesseCandlestick> candles = new ArrayList<>();

        JapanesseCandlestick candle;
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"), 119.50999,119.50999,114.65,126.49567876,0.08171997);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"), 119.50999,119.50999,114.65,126.49567876,0.08171997);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"), 0.0,0.0,114.69,126.55253002,0.0);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"), 119.00061502,119.00061502,114.69,126.5,0.1);
        candles.add(candle);

        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"), 119.50999,119.50999,114.65,126.49567876,0.08171997);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:49:55Z"), Instant.parse("2018-12-02T19:50:55Z"), 0.0,0.0,114.69,126.55253002,0.0);
        candles.add(candle);


        Predictor predictor = new MovingArrangePredictor("/media/alexander/Новый том/Repositore/Java/Algotrading");
        System.out.println(predictor.nextCandle(candles));
    }
}
