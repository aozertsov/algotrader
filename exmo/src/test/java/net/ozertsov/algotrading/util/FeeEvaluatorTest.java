package net.ozertsov.algotrading.util;

import org.junit.Assert;
import org.junit.Test;
import org.knowm.xchange.dto.Order;

import java.math.BigDecimal;

public class FeeEvaluatorTest {

    @Test
    public void checkASKFee() {
        BigDecimal calculatedFee = FeeEvaluator.calculateFee(BigDecimal.valueOf(0.08105365), Order.OrderType.ASK, BigDecimal.valueOf(125.4), BigDecimal.valueOf(0.002));
        BigDecimal expectedFee = BigDecimal.valueOf(0.02032826);
        Assert.assertEquals(expectedFee, calculatedFee);
    }

    @Test
    public void checkBIDFee() {
        BigDecimal calculatedFee = FeeEvaluator.calculateFee(BigDecimal.valueOf(0.08105365), Order.OrderType.BID, BigDecimal.valueOf(125.4), BigDecimal.valueOf(0.002));
        BigDecimal expectedFee = BigDecimal.valueOf(0.00016211);
        Assert.assertEquals(expectedFee, calculatedFee);
    }
}
