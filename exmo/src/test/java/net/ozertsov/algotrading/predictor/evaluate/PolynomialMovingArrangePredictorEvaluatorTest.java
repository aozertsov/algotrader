package net.ozertsov.algotrading.predictor.evaluate;

import com.google.common.io.Resources;
import net.ozertsov.algotrading.io.HistoryReader;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import net.ozertsov.algotrading.predict.evaluate.EvaluationException;
import net.ozertsov.algotrading.predict.evaluate.PolynomialMovingArrangePredictEvaluator;
import net.ozertsov.algotrading.predict.evaluate.PredictEvaluator;
import net.ozertsov.junit.CustomAssert;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class PolynomialMovingArrangePredictorEvaluatorTest {
    @Test
    public void testNonCrushed() throws URISyntaxException, EvaluationException {
        PredictEvaluator evaluator = new PolynomialMovingArrangePredictEvaluator(new double[] {0.7, 0.5});

        URL uri = Resources.getResource("crowlerTestDataSet.csv");
        HistoryReader reader = new HistoryReader(new File(uri.toURI()));
        List<JapanesseCandlestick> candlestickList = reader.readHistory();

        evaluator.predictCandle(candlestickList.subList(0, 435));

    }

    @Test
    public void testAllCandlesEqualsAllWeightsEqual() throws EvaluationException {
        PredictEvaluator evaluator = new PolynomialMovingArrangePredictEvaluator(new double[] {0.2, 0.2, 0.2, 0.2, 0.2});

        List<JapanesseCandlestick> candles = new ArrayList<>();

        JapanesseCandlestick candle;
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                120,130,114,126,1);
        candles.add(candle);



        JapanesseCandlestick predictedCandle = evaluator.predictCandle(candles);

        JapanesseCandlestick expectedCandle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"),
                120,130,114,126,1);

        CustomAssert.assertEquals(expectedCandle, predictedCandle);
    }

    @Test
    public void testAllCandlesEqualsWeightsNonEqual() throws EvaluationException {
        PredictEvaluator evaluator = new PolynomialMovingArrangePredictEvaluator(new double[] {0.2, 0.2, 0.2, 0.2, 0.3});

        List<JapanesseCandlestick> candles = new ArrayList<>();

        JapanesseCandlestick candle;
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                120,130,114,126,1);
        candles.add(candle);



        JapanesseCandlestick predictedCandle = evaluator.predictCandle(candles);

        JapanesseCandlestick expectedCandle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"),
                132,143,125.39999999999999,138.60000000000002,1.0999999999999999);

        CustomAssert.assertEquals(expectedCandle, predictedCandle);
    }
}
