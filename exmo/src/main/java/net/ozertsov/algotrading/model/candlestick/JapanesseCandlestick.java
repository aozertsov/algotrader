package net.ozertsov.algotrading.model.candlestick;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import net.ozertsov.algotrading.io.Recordable;
import net.ozertsov.algotrading.io.Schema;

import java.time.Instant;
import java.util.Arrays;

import static net.ozertsov.algotrading.io.Schema.Type.*;
import static net.ozertsov.algotrading.model.candlestick.CandlestickType.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class JapanesseCandlestick implements Recordable {
    @Getter
    Instant from; //date start see trading operation
    @Getter
    Instant to; //date end see trading operation
    @Getter
    CandlestickType type; //result of marked price in period
    @Getter
    double openPrice; //first order price
    @Getter
    double closePrice; //last order price
    @Getter
    double lowPrice; //low order in orderbook in period
    @Getter
    double highPrice; //high order in orderbook in period
    @Getter
    double volume; //trding volume (in currency)

    public JapanesseCandlestick(Instant from, Instant to, double lastPrice,
                                double lowPrice, double highPrice, double volume) {
        this(from, to, lowPrice, highPrice, volume);
        this.openPrice = lastPrice;
        this.closePrice = lastPrice;
        type = EMPTY;
    }

    public JapanesseCandlestick(Instant from, Instant to,
                                double lowPrice, double highPrice, double volume) {
        this.from = from;
        this.to = to;
        this.lowPrice = lowPrice;
        this.highPrice = highPrice;
        this.volume = volume;
    }


    public JapanesseCandlestick(Instant from, Instant to,
                                double openPrice, double closePrice,
                                double lowPrice, double highPrice, double volume) {
        this(from, to, lowPrice, highPrice, volume);
        this.openPrice = openPrice;
        this.closePrice = closePrice;
        if (openPrice == 0.0 && closePrice == 0.0)
            type = EMPTY;
        else
            type = openPrice <= closePrice ? (openPrice < closePrice ? LONG : FIXED) : SHORT;
    }


    @Override
    public Schema getSchema() {
        return new Schema(Arrays.asList(
                new Schema.Field("from", INSTANT),
                new Schema.Field("to", INSTANT),
                new Schema.Field("type", ENUM),
                new Schema.Field("openPrice", DOUBLE),
                new Schema.Field("closePrice", DOUBLE),
                new Schema.Field("lowPrice", DOUBLE),
                new Schema.Field("highPrice", DOUBLE),
                new Schema.Field("volume", DOUBLE)
        ));
    }

    @Override
    public String getData() {
        return String.format("%s,%s,%s,%s,%s,%s,%s,%s", this.getFrom(), this.getTo(), this.getType(),
                this.getOpenPrice(), this.getClosePrice(), this.getLowPrice(), this.getHighPrice(), this.getVolume());
    }
}
