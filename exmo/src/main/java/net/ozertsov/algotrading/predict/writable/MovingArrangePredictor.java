package net.ozertsov.algotrading.predict.writable;

import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.io.HistoryWriter;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import net.ozertsov.algotrading.predict.evaluate.EvaluationException;
import net.ozertsov.algotrading.predict.evaluate.MovingArrangePredictEvaluator;

import java.util.List;

/**
 * Класс, который генерирует предсказание по принципу метода скользящего среднего
 * (суммируем слагаемые и делим на количество слагаемых)
 */
@Log4j2
public class MovingArrangePredictor extends WritablePredictor {

    public MovingArrangePredictor(String folder) {
        this(3, folder, "predictions_arrange", "");
    }

    public MovingArrangePredictor(int selectedCount, String folder, String filePrefix, String fileSuffix) {
        super(new MovingArrangePredictEvaluator(),
                HistoryWriter.createDocument(folder, filePrefix + "_" + fileSuffix));
        this.selectedCount = selectedCount;
    }

    /**
     * метод, которые делает предсказание на готовых данных
     * @param candles
     * @return
     */
    public JapanesseCandlestick predictCandle(List<JapanesseCandlestick> candles) {
        try {
            return evaluator.predictCandle(candles);
        } catch (EvaluationException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<JapanesseCandlestick> nextCandle(List<JapanesseCandlestick> candles) {
        List<JapanesseCandlestick> candle = super.nextCandle(candles);
        writer.appendData(candle);
        return candle;
    }

    @Override
    public JapanesseCandlestick changeInvalidCandle(List<JapanesseCandlestick> candles) {
        return predictCandle(generateOptimalData(candles));
    }
}