package net.ozertsov.algotrading.util;

import java.util.concurrent.TimeUnit;

public class FileNamingUtil {

    public static String generateFileName(String prefix, String format, int timeout, TimeUnit timeoutType) {
        return String.format("%s_%s_%s.%s", prefix, timeoutType.toString().toLowerCase(), timeout, format);
    }
}
