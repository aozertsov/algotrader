package net.ozertsov.algotrading.model;

import lombok.experimental.FieldDefaults;
import net.ozertsov.algotrading.io.HistoryWriter;
import net.ozertsov.algotrading.io.Recordable;

import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import static lombok.AccessLevel.PROTECTED;

/**
 * Класс, который хранит данные. Периодически сбрасывает данные на диск.
 */
@FieldDefaults(level = PROTECTED)
public abstract class HistoryKeeper<T extends Recordable> {
    final HistoryWriter writer;
    final int bufferSize;
    final int keepInMemory;
    final AtomicInteger currentUnbuffered;
    List<T> data = new CopyOnWriteArrayList<>();


    public HistoryKeeper(int keepInMemory, int bufferSize, File file) {
        this.writer = HistoryWriter.createDocument(file);
        this.currentUnbuffered = new AtomicInteger(0);
        if (bufferSize <= 0)
            throw new IllegalArgumentException("bufferSize should be higher than 0");
        this.bufferSize = bufferSize;
        this.keepInMemory = keepInMemory;

//        Schema schema = ReflectData.get().getSchema(T);
    }

    public abstract void addElement(T candle);

    public List<T> getElements() {
        return data;
    }

    public T getLast() {
        return data.get(data.size() - 1);
    }
}
