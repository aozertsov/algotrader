//package net.ozertsov.algotrading.predict.ml
//
//import org.apache.spark.ml.feature.VectorAssembler
//import org.apache.spark.ml.regression.{LinearRegression, LinearRegressionModel, LinearRegressionSummary}
//import org.apache.spark.sql.{Dataset, Encoders, Row, SparkSession}
//import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
//
//object RegressionPredictor extends App {
//
//  val spark = SparkSession.builder.master("local[*]").getOrCreate
//
//  val schema = new StructType(Array[StructField](
//    DataTypes.createStructField("from", DataTypes.TimestampType, true),
//    DataTypes.createStructField("to", DataTypes.TimestampType, true),
//    DataTypes.createStructField("type", DataTypes.StringType, true),
//    DataTypes.createStructField("openPrice", DataTypes.DoubleType, true),
//    DataTypes.createStructField("closePrice", DataTypes.DoubleType, true),
//    DataTypes.createStructField("lowPrice", DataTypes.DoubleType, true),
//    DataTypes.createStructField("highPrice", DataTypes.DoubleType, true),
//    DataTypes.createStructField("volume", DataTypes.DoubleType, true),
//    DataTypes.createStructField("openPredictPrice", DataTypes.DoubleType, true)))
//
//  val training1 = spark.read.option("header", "true").schema(schema).csv("D:\\Project\\Algotrading\\exmo\\crowler_minutes_10.csv")
//  //                .option("charset", "UTF8")
//  //                .option("delimiter",",")
//  //                .csv("/media/alexander/Новый том/Repositore/Java/Algotrading/crowler_minutes_1.csv");
//
//
//
//
//  import spark.implicits._
//  val training = training1.as[PredCandle]
//    .filter("volume > 0.0")
//    .filter("openPredictPrice > 0.0")
//
//  training.show()
//  training.printSchema()
//
//  val array = Array(0.8, 0.2)
//
//  val splittedDatas = training.randomSplit(array)
//
//  val inputCols = Array("from", "to", "openPrice", "closePrice", "lowPrice", "highPrice", "volume")
//  val featured = new VectorAssembler().setInputCols(inputCols).setOutputCol("features").transform(splittedDatas(0))
//
//  featured.printSchema()
//
//  val regression = new LinearRegression().setMaxIter(10).setRegParam(0.3).setElasticNetParam(0.8).setFeaturesCol("features").setLabelCol("openPredictPrice")
//
//
//  val model = regression.fit(featured)
//  val testFeatured = new VectorAssembler().setInputCols(inputCols).setOutputCol("features").transform(splittedDatas(1))
//
//  println("FeaturedDatas:")
//  testFeatured.show()
//
//  val a = model.transform(testFeatured)
//
//  a.show()
//
//  val summary = model.evaluate(testFeatured)
//  println(summary.meanAbsoluteError)
//  println(summary.meanSquaredError)
//}
