package net.ozertsov.algotrading.predict;

import net.ozertsov.algotrading.model.candlestick.CandlestickType;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import net.ozertsov.algotrading.predict.evaluate.PredictEvaluator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Класс, который на основе японских свечей генерирует предсказания
 */
public abstract class Predictor {

    protected PredictEvaluator evaluator;
    protected int selectedCount = 0;

    protected Predictor(PredictEvaluator evaluator) {
        this.evaluator = evaluator;
    }

    /**
     * Внешний метод, который готовит данныые и выдает предикт
     * @param candles
     * @return
     */
    public List<JapanesseCandlestick> nextCandle(List<JapanesseCandlestick> candles) {
        return Collections.singletonList(predictCandle(generateOptimalData(candles)));
    }

    /**
     * Метод, который меняет сломанную свечу на что-то вменяемое
     * @param candles
     * @return
     */
    public abstract JapanesseCandlestick changeInvalidCandle(List<JapanesseCandlestick> candles);

    public void updatePredict(List<JapanesseCandlestick> candles) {}

    public List<JapanesseCandlestick> generateOptimalData(List<JapanesseCandlestick> candles) {
        List<JapanesseCandlestick> candlesList = new ArrayList<>();

        for (int i = candles.size() - 1; i > -1 && i > candles.size() - selectedCount - 1; i--) {
            if (isShouldBeExcluded(candles.get(i))) {
                JapanesseCandlestick candle = changeInvalidCandle(candles.subList(0, i));
                candlesList.add(candle);
            }
            else {
                candlesList.add(candles.get(i));
            }
        }
        Collections.reverse(candlesList);
        return candlesList;
    }

    /**
     * Внутренний метод, который выдает предикт на подготовленных данных
     * @param candles
     * @return
     */
    protected abstract JapanesseCandlestick predictCandle(List<JapanesseCandlestick> candles);

    protected boolean isShouldBeExcluded(JapanesseCandlestick candle) {
        return candle.getType() == CandlestickType.EMPTY;
    }
}
