package net.ozertsov.algotrading.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Pair<K, V> {
    @Getter
    K key;
    @Setter
    V value;
}
