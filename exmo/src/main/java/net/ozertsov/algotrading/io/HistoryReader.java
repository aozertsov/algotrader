package net.ozertsov.algotrading.io;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;

import java.io.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class HistoryReader {

    File file;

    public HistoryReader(File file) {
        this.file = file;
    }

    public List<JapanesseCandlestick> readHistory() {
        List<JapanesseCandlestick> history = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null && line != "") {
                String[] datas = line.split(",");
                JapanesseCandlestick candle = new JapanesseCandlestick(Instant.parse(datas[0]), Instant.parse(datas[1]),
                        Double.parseDouble(datas[3]), Double.parseDouble(datas[4]),
                        Double.parseDouble(datas[5]), Double.parseDouble(datas[6]), Double.parseDouble(datas[7]));
                history.add(candle);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return history;
    }
}
