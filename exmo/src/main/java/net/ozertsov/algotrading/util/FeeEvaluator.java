package net.ozertsov.algotrading.util;

import org.knowm.xchange.dto.Order;

import java.math.BigDecimal;
import java.math.MathContext;

import static java.math.RoundingMode.HALF_UP;

public class FeeEvaluator {

    /**
     * считает размер комиссии. Для покупок - в первой части пары, для продажи - во второй.
     * @param originalAmount
     * @param type
     * @param price
     * @param feePercent
     * @return
     */
    public static BigDecimal calculateFee(BigDecimal originalAmount, Order.OrderType type, BigDecimal price, BigDecimal feePercent) {
        BigDecimal result = BigDecimal.ZERO;
        if (type == Order.OrderType.ASK) {
            result = originalAmount.multiply(price).multiply(feePercent);
        } else {
            result = originalAmount.multiply(feePercent);
        }
        result = result.setScale(8,HALF_UP);
        return result;
    }

}
