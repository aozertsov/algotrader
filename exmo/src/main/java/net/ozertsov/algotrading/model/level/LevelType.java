package net.ozertsov.algotrading.model.level;

public enum LevelType {
    SUPPORT,
    RESISTANCE
}
