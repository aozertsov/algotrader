package net.ozertsov.algotrading.io;

public interface Recordable {

    Schema getSchema();
    String getData();
}
