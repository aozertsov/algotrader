package net.ozertsov.algotrading.model.history;

import net.ozertsov.algotrading.io.HistoryReader;
import net.ozertsov.algotrading.model.HistoryKeeper;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CandleKeeperTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testWritingOrder() throws IOException, InterruptedException {
        File f = folder.newFile();
        HistoryKeeper keeper = new CandleKeeper(0, 2, f);

        JapanesseCandlestick c1 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c2 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c3 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);

        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
        queue.add(() -> keeper.addElement(c1));
        queue.add(() -> keeper.addElement(c2));
        queue.add(() -> keeper.addElement(c3));

        ThreadPoolExecutor pool = new ThreadPoolExecutor(3, 4, 3, TimeUnit.MINUTES, queue);
        pool.prestartAllCoreThreads();

        pool.shutdown();
        pool.awaitTermination(2, TimeUnit.MINUTES);

        HistoryReader reader = new HistoryReader(f);
        List<JapanesseCandlestick> candlestickList = reader.readHistory();
        Assert.assertEquals(2, candlestickList.size());
        Assert.assertEquals(1, keeper.getElements().size());
    }

    @Test
    public void testWritingSeveralTimes() throws IOException, InterruptedException {
        File f = folder.newFile();
        CandleKeeper keeper = new CandleKeeper(0, 2, f);

        JapanesseCandlestick c1 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c2 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c3 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c4 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c5 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);

        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
        queue.add(() -> keeper.addElement(c1));
        queue.add(() -> keeper.addElement(c2));
        queue.add(() -> keeper.addElement(c3));
        queue.add(() -> keeper.addElement(c4));
        queue.add(() -> keeper.addElement(c5));

        ThreadPoolExecutor pool = new ThreadPoolExecutor(5, 5, 3, TimeUnit.MINUTES, queue);
        pool.prestartAllCoreThreads();

        pool.shutdown();
        pool.awaitTermination(2, TimeUnit.MINUTES);

        HistoryReader reader = new HistoryReader(f);
        List<JapanesseCandlestick> candlestickList = reader.readHistory();
        Assert.assertEquals(4, candlestickList.size());
        Assert.assertEquals(1, keeper.getElements().size());
    }

    @Test
    public void testWritingKeepInMemory() throws IOException, InterruptedException {
        File f = folder.newFile();
        CandleKeeper keeper = new CandleKeeper(2, 4, f);

        JapanesseCandlestick c1 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c2 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c3 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c4 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c5 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);

        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
        queue.add(() -> keeper.addElement(c1));
        queue.add(() -> keeper.addElement(c2));
        queue.add(() -> keeper.addElement(c3));
        queue.add(() -> keeper.addElement(c4));
        queue.add(() -> keeper.addElement(c5));

        ThreadPoolExecutor pool = new ThreadPoolExecutor(5, 5, 3, TimeUnit.MINUTES, queue);
        pool.prestartAllCoreThreads();

        pool.shutdown();
        pool.awaitTermination(2, TimeUnit.MINUTES);

        HistoryReader reader = new HistoryReader(f);
        List<JapanesseCandlestick> candlestickList = reader.readHistory();
        Assert.assertEquals(2, candlestickList.size());
        Assert.assertEquals(3, keeper.getElements().size());
    }

    @Test
    public void testWritingSeveralTimesKeepInMemory() throws IOException, InterruptedException {
        File f = folder.newFile();
        CandleKeeper keeper = new CandleKeeper(2, 4, f);

        JapanesseCandlestick c1 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c2 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c3 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c4 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c5 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c6 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);
        JapanesseCandlestick c7 = new JapanesseCandlestick(Instant.parse("2018-12-02T19:49:55Z"), Instant.parse("2018-12-02T19:50:55Z"),
                119.50999,119.50999,114.65,126.49567876,0.08171997);

        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
        queue.add(() -> keeper.addElement(c1));
        queue.add(() -> keeper.addElement(c2));
        queue.add(() -> keeper.addElement(c3));
        queue.add(() -> keeper.addElement(c4));
        queue.add(() -> keeper.addElement(c5));
        queue.add(() -> keeper.addElement(c6));
        queue.add(() -> keeper.addElement(c7));

        ThreadPoolExecutor pool = new ThreadPoolExecutor(7, 7, 3, TimeUnit.MINUTES, queue);
        pool.prestartAllCoreThreads();

        pool.shutdown();
        pool.awaitTermination(2, TimeUnit.MINUTES);

        HistoryReader reader = new HistoryReader(f);
        List<JapanesseCandlestick> candlestickList = reader.readHistory();
        Assert.assertEquals(4, candlestickList.size());
        Assert.assertEquals(3, keeper.getElements().size());
    }
}
