package net.ozertsov.algotrading.predictor.evaluate;

import com.google.common.io.Resources;
import net.ozertsov.algotrading.io.HistoryReader;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import net.ozertsov.algotrading.predict.evaluate.EvaluationException;
import net.ozertsov.algotrading.predict.evaluate.MovingArrangePredictEvaluator;
import net.ozertsov.algotrading.predict.evaluate.PredictEvaluator;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class MovingArrangePredictEvaluatorTest {

    @Test
    public void testNonCrushed() throws URISyntaxException, EvaluationException {
        PredictEvaluator evaluator = new MovingArrangePredictEvaluator();

        URL uri = Resources.getResource("crowlerTestDataSet.csv");
        HistoryReader reader = new HistoryReader(new File(uri.toURI()));
        List<JapanesseCandlestick> candlestickList = reader.readHistory();

        evaluator.predictCandle(candlestickList.subList(0, 435));

    }

    @Test
    public void testAllCandlesEquals() throws EvaluationException {
        PredictEvaluator evaluator = new MovingArrangePredictEvaluator();

        List<JapanesseCandlestick> candles = new ArrayList<>();

        JapanesseCandlestick candle;
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:49:55Z"), Instant.parse("2018-12-02T19:50:55Z"),
                120,130,114,126,1);
        candles.add(candle);
        
        
        JapanesseCandlestick predictedCandle = evaluator.predictCandle(candles);
        
        JapanesseCandlestick expectedCandle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:50:55Z"), Instant.parse("2018-12-02T19:51:55Z"),
                120,130,114,126,1);

        Assert.assertEquals(expectedCandle, predictedCandle);
    }

    @Test
    public void testOpenCloseDifferent() throws EvaluationException {
        PredictEvaluator evaluator = new MovingArrangePredictEvaluator();

        List<JapanesseCandlestick> candles = new ArrayList<>();

        JapanesseCandlestick candle;
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                125,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                125,130,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                115,135,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                115,135,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                120,125,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"),
                120,125,114,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:49:55Z"), Instant.parse("2018-12-02T19:50:55Z"),
                120,130,114,126,1);
        candles.add(candle);


        JapanesseCandlestick predictedCandle = evaluator.predictCandle(candles);

        JapanesseCandlestick expectedCandle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:50:55Z"), Instant.parse("2018-12-02T19:51:55Z"),
                120,130,114,126,1);

        Assert.assertEquals(expectedCandle, predictedCandle);
    }

    @Test
    public void testLowHighPricesDifferent() throws EvaluationException {
        PredictEvaluator evaluator = new MovingArrangePredictEvaluator();

        List<JapanesseCandlestick> candles = new ArrayList<>();

        JapanesseCandlestick candle;
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                120,130,110,126,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                120,130,110,122,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                120,130,116,128,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                120,130,116,128,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                120,130,116,128,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"),
                120,130,116,128,1);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:49:55Z"), Instant.parse("2018-12-02T19:50:55Z"),
                120,130,114,122,1);
        candles.add(candle);


        JapanesseCandlestick predictedCandle = evaluator.predictCandle(candles);

        JapanesseCandlestick expectedCandle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:50:55Z"), Instant.parse("2018-12-02T19:51:55Z"),
                120,130,114,126,1);

        Assert.assertEquals(expectedCandle, predictedCandle);
    }

    @Test
    public void testVolumeDifferent() throws EvaluationException {
        PredictEvaluator evaluator = new MovingArrangePredictEvaluator();

        List<JapanesseCandlestick> candles = new ArrayList<>();

        JapanesseCandlestick candle;
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:43:55Z"), Instant.parse("2018-12-02T19:44:55Z"),
                120,130,114,126,4);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:44:55Z"), Instant.parse("2018-12-02T19:45:55Z"),
                120,130,114,126,0.5);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:45:55Z"), Instant.parse("2018-12-02T19:46:55Z"),
                120,130,114,126,0.5);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:46:55Z"), Instant.parse("2018-12-02T19:47:55Z"),
                120,130,114,126,0.5);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:47:55Z"), Instant.parse("2018-12-02T19:48:55Z"),
                120,130,114,126,0.5);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:48:55Z"), Instant.parse("2018-12-02T19:49:55Z"),
                120,130,114,126,0.5);
        candles.add(candle);
        candle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:49:55Z"), Instant.parse("2018-12-02T19:50:55Z"),
                120,130,114,126,0.5);
        candles.add(candle);


        JapanesseCandlestick predictedCandle = evaluator.predictCandle(candles);

        JapanesseCandlestick expectedCandle = new JapanesseCandlestick(Instant.parse("2018-12-02T19:50:55Z"), Instant.parse("2018-12-02T19:51:55Z"),
                120,130,114,126,1);

        Assert.assertEquals(expectedCandle, predictedCandle);
    }
}
