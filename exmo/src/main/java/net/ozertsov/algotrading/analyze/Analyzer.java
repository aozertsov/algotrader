package net.ozertsov.algotrading.analyze;

import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;

import java.util.Collection;

public interface Analyzer {
    double getSupportPrice(Collection<JapanesseCandlestick> candles);
    double getResistancePrice(Collection<JapanesseCandlestick> candles);
}
