package net.ozertsov.algotrading;

import lombok.extern.log4j.Log4j2;
import org.knowm.xchange.exmo.ExmoExchange;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.knowm.xchange.currency.CurrencyPair.ETH_USD;

@Log4j2
public class Starter {
    public static void main(String[] args) throws IOException, InterruptedException {

        log.info("Started");
//        ExchangeSpecification specification = new ExchangeSpecification(ExmoExchange.class);
//        specification.setApiKey("K-4a01c6acb55c9183afcc3186a456804f3ad39353");
//        specification.setSecretKey("S-f812621cb14efce1f1e5bf2acf7f822b3590b1d4");
//        Exchange ex = ExchangeFactory.INSTANCE.createExchange(specification);
//
//        AccountService as = ex.getAccountService();
//        List<FundingRecord> list = as.getFundingHistory(new ExmoFundingHistoryParams(new Date(2019, 02, 14)));
//        System.out.println(list);
//
//        TradeService tradeService = ex.getTradeService();
//        ExmoTradeHistoryParams a = new ExmoTradeHistoryParams();
//                a.setCurrencyPairs(Collections.singleton(ETH_USD));
//        System.out.println(tradeService.getTradeHistory(a));

//        List<Order> orders = tradeService.getOpenOrders()
//                .getAllOpenOrders()
//                .stream()
//                .filter(x -> x.getCurrencyPair().equals(ETH_USD))
//                .collect(Collectors.toList());
//        System.out.println(orders);
//

        String directory = "../tradeFiles";
        Worker w_1 = new Worker(ETH_USD, ExmoExchange.class, directory, 0.7, 1, TimeUnit.MINUTES);
        Worker w_3 = new Worker(ETH_USD, ExmoExchange.class, directory, 0.7, 3, TimeUnit.MINUTES);
        Worker w_5 = new Worker(ETH_USD, ExmoExchange.class, directory, 0.7, 5, TimeUnit.MINUTES);
        Worker w_7 = new Worker(ETH_USD, ExmoExchange.class, directory, 0.7, 7, TimeUnit.MINUTES);
        Worker w_10 = new Worker(ETH_USD, ExmoExchange.class, directory, 0.7, 10, TimeUnit.MINUTES);
//        Worker w = new Worker(ETH_USD, ExmoExchange.class, "/home/ubuntu/algotrading/docs", 5, TimeUnit.MINUTES);

//        Thread.sleep(300000);

//        System.out.println("Stages");
//        System.out.println(s.getSupportResistance(c.getTradingPair(), from.getEpochSecond(), Instant.now().getEpochSecond()));

//        Starter a = new Starter();
//        a.wait();
    }
}