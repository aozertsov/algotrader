//package net.ozertsov.algotrading.predict.ml;
//
//import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
//import net.ozertsov.algotrading.predict.Predictor;
//import org.apache.spark.api.java.function.FilterFunction;
//import org.apache.spark.ml.PredictionModel;
//import org.apache.spark.ml.feature.VectorAssembler;
//import org.apache.spark.ml.regression.*;
//import org.apache.spark.mllib.regression.LinearRegressionWithSGD;
//import org.apache.spark.sql.Dataset;
//import org.apache.spark.sql.Encoders;
//import org.apache.spark.sql.Row;
//import org.apache.spark.sql.SparkSession;
//import org.apache.spark.sql.types.*;
//
//import java.util.Collection;
//import java.util.List;
//
//public class LinearRegressionBasedPredictor extends Predictor {
//
//    public static void main(String[] args) {
//        LinearRegressionBasedPredictor predictor = new LinearRegressionBasedPredictor();
//        predictor.a();
//
//    }
//    @Override
//    public JapanesseCandlestick nextCandle(List<JapanesseCandlestick> candles) {
//        return null;
//    }
//
//    @Override
//    public boolean isNeedBuy(Collection<JapanesseCandlestick> candles) {
//        return false;
//    }
//
//    @Override
//    public boolean isNeedSell(Collection<JapanesseCandlestick> candles) {
//        return false;
//    }
//
//    @Override
//    protected JapanesseCandlestick predictCandle(List<JapanesseCandlestick> candles) {
//        return null;
//    }
//
//    @Override
//    public JapanesseCandlestick changeInvalidCandle(List<JapanesseCandlestick> candles) {
//        return predictCandle(generateOptimalData(candles));
//    }
//
//
//    public void a() {
//        SparkSession  spark = SparkSession.builder()
//                .master("local[*]")
//                .getOrCreate();
//
//        final StructType schema = new StructType(new StructField[] {
//                DataTypes.createStructField("from",  DataTypes.StringType, true),
//                DataTypes.createStructField("to",  DataTypes.StringType, true),
//                DataTypes.createStructField("type",  DataTypes.StringType, true),
//                DataTypes.createStructField("openPrice",  DataTypes.DoubleType, true),
//                DataTypes.createStructField("closePrice",  DataTypes.DoubleType, true),
//                DataTypes.createStructField("lowPrice",  DataTypes.DoubleType, true),
//                DataTypes.createStructField("highPrice",  DataTypes.DoubleType, true),
//                DataTypes.createStructField("volume",  DataTypes.DoubleType, true),
//                DataTypes.createStructField("openPredictPrice",  DataTypes.DoubleType, true)
//        });
//        Dataset<Row> training1 = spark.read()
//                .option("header", "true")
//                .schema(schema)
////                .option("charset", "UTF8")
////                .option("delimiter",",")
//                .csv("/media/alexander/Новый том/Repositore/Java/Algotrading/exmo/crowler_minutes_10.csv");
////                .csv("D:\\Project\\Algotrading\\exmo\\crowler_minutes_10.csv");
//
//
//
//        Dataset<PredictionCandle> training = training1.as(Encoders.bean(PredictionCandle.class))
//                .filter("volume > 0.0")
//                .filter("openPredictPrice > 0.0");
//
////        training.show();
//
//        double[] array = {0.8, 0.2};
//
//        Dataset<PredictionCandle>[] splittedDatas = training.randomSplit(array);
//
//        String[] inputCols = {"to", "openPrice", "closePrice", "lowPrice", "highPrice", "volume"};
//        Dataset<Row> featured = new VectorAssembler()
//                .setInputCols(inputCols)
//                .setOutputCol("features")
//                .transform(splittedDatas[0]);
//
//        featured.printSchema();
//
//        GeneralizedLinearRegression generalizedRegression = new GeneralizedLinearRegression()
//                .setFamily("gamma")
//                .setMaxIter(1000)
//                .setRegParam(0.1)
//                .setFeaturesCol("features")
//                .setLabelCol("openPredictPrice");
//
//        LinearRegression regression = new  LinearRegression()
//                .setMaxIter(1000)
//                .setRegParam(0.1)
//                .setElasticNetParam(0.8)
//                .setFeaturesCol("features")
//                .setLabelCol("openPredictPrice");
//
//
//        LinearRegressionModel model = regression.fit(featured);
//        GeneralizedLinearRegressionModel generalizedModel = generalizedRegression.fit(featured);
//        Dataset<Row> testFeatured = new VectorAssembler()
//                .setInputCols(inputCols)
//                .setOutputCol("features")
//                .transform(splittedDatas[1]);
//
//        System.out.println("FeaturedDatas:");
//        testFeatured.show();
//
//        Dataset<Row> a = model.transform(testFeatured);
//        Dataset<Row> gen = generalizedModel.transform(testFeatured);
//
//        a.show();
//        gen.show();
//
//        LinearRegressionSummary summary = model.evaluate(testFeatured);
//        GeneralizedLinearRegressionSummary genSummary = generalizedModel.evaluate(testFeatured);
//        System.out.println(summary.meanAbsoluteError());
//        System.out.println(summary.meanSquaredError());
//        System.out.println("generized:");
//        System.out.println(genSummary.dispersion());
//        gen.show();
//    }
//}
