package net.ozertsov.algotrading.predict.evaluate;

import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;

import java.time.Instant;
import java.util.List;

public class MovingArrangePredictEvaluator extends PredictEvaluator {

    @Override
    public JapanesseCandlestick predictCandle(List<JapanesseCandlestick> candles) throws EvaluationException {
        int count = candles.size();
        double openPrice = candles.stream().map(x -> x.getOpenPrice()).reduce(0d, (x, y) -> x + y) / count;
        double closePrice = candles.stream().map(x -> x.getClosePrice()).reduce(0d, (x, y) -> x + y) / count;
        double lowPrice = candles.stream().map(x -> x.getLowPrice()).reduce(0d, (x, y) -> x + y) / count;
        double highPrice = candles.stream().map(x -> x.getHighPrice()).reduce(0d, (x, y) -> x + y) / count;
        double volume = candles.stream().map(x -> x.getVolume()).reduce(0d, (x, y) -> x + y) / count;

        Instant from = candles.get(candles.size() - 1).getTo();
        Instant to = candles.get(candles.size() - 1).getTo().plusSeconds(candles.get(candles.size() - 1).getTo().getEpochSecond() - candles.get(candles.size() - 1).getFrom().getEpochSecond());

        JapanesseCandlestick predictedCandle = new JapanesseCandlestick(from, to, openPrice, closePrice, lowPrice, highPrice, volume);
        return predictedCandle;
    }
}
