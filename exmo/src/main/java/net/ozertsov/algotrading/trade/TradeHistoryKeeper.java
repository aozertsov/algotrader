package net.ozertsov.algotrading.trade;

import lombok.ToString;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@ToString
public class TradeHistoryKeeper {

    Map<String, HistoricalOrder> history = new ConcurrentHashMap();

    public void removeOrder(String orderId) {
        history.remove(orderId);
    }

    public void add(String id, HistoricalOrder historicalOrder) {
        history.put(id, historicalOrder);
    }

    public void removeAll(Collection<String> orderIds) {
        for (String id : orderIds) {
            history.remove(id);
        }
    }

    public Set<Map.Entry<String, HistoricalOrder>> getTrades() {
        return history.entrySet();
    }

    public Collection<String> getIds(){
        return  history.keySet();
    }
}