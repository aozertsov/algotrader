package net.ozertsov.algotrading.predict.writable.movingarrange;

import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LongHorizontPolynomialMovingArangePredictor extends PolynomialMovingArrangePredictor {
    final int planningHorizont;

    public LongHorizontPolynomialMovingArangePredictor(String folder, int degree, double[] coeffs, int horizont) {
        super(folder, "long_horizont_moving_arrange_predictor", degree, coeffs);
        this.planningHorizont = horizont;
    }

    @Override
    public List<JapanesseCandlestick> nextCandle(List<JapanesseCandlestick> candles) {
        List<JapanesseCandlestick> internalList = candles.stream()
                .collect(Collectors.toList());
        List<JapanesseCandlestick> result = new ArrayList<>();
        for (int i = 0; i < planningHorizont; i++) {
            JapanesseCandlestick candle = predictCandle(generateOptimalData(internalList));
            internalList.add(candle);
            result.add(candle);
        }
        return result;
    }
}
