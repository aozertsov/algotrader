package net.ozertsov.algotrading;


import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.analyze.Analyzer;
import net.ozertsov.algotrading.analyze.ChunkBasedAnalyzer;
import net.ozertsov.algotrading.crowl.AbstractCrowler;
import net.ozertsov.algotrading.crowl.ExmoCrowler;
import net.ozertsov.algotrading.model.history.CandleKeeper;
import net.ozertsov.algotrading.predict.Predictor;
import net.ozertsov.algotrading.predict.writable.movingarrange.PolynomialMovingArrangePredictor;
import net.ozertsov.algotrading.trade.Trader;
import org.knowm.xchange.currency.CurrencyPair;

import java.io.File;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static net.ozertsov.algotrading.util.FileNamingUtil.generateFileName;

@Log4j2
public class Worker {
    final CurrencyPair pair;
    final Class exchangeClass;
    final String directory;
    final String historyFileName;

    CandleKeeper candleKeeper;
    AbstractCrowler crowler;
    Analyzer analyzer;
    double tradeDifference;
    Predictor predictor;
    Predictor predictor2;
    ScheduledExecutorService crowlerService;
    ScheduledExecutorService predictorService;
    Trader trader;

    public Worker(CurrencyPair pair, Class exchangeClass, String directory, double tradeDifference, int timeout, TimeUnit unit) {
        log.info(() -> "Start worker at: " + Instant.now());
        log.info(() -> "main sout directory is" + directory);
        this.pair = pair;
        this.exchangeClass = exchangeClass;
        this.directory = directory;
        this.tradeDifference = tradeDifference;
        trader = new Trader(exchangeClass, pair, 0, tradeDifference, 0, 10,
                200, 0, new File(directory, generateFileName("trade_history", "csv", timeout, unit)));
        historyFileName = generateFileName("history", "csv", timeout, unit);
        candleKeeper = new CandleKeeper(10, 20, Paths.get(directory, historyFileName).toFile());
        crowler = new ExmoCrowler(pair, exchangeClass, candleKeeper);
        analyzer = new ChunkBasedAnalyzer();
        String folder = directory + "/predict";
//        predictor = new PolynomialMovingArrangePredictor(folder, 5, new double[] {0.4, 0.3, 0.12, 0.1, 0.08});
        predictor = new PolynomialMovingArrangePredictor(folder, timeout + "_" + unit, 5, new double[] {0.45, 0.3, 0.1, 0.1, 0.05});
        crowlerService = Executors.newSingleThreadScheduledExecutor();
        predictorService = Executors.newScheduledThreadPool(2);
        crowlerService.scheduleWithFixedDelay(() -> crowler.updateHistory(), timeout, timeout, unit);
        predictorService.scheduleWithFixedDelay(() -> trader.trade(predictor.nextCandle(candleKeeper.getElements()), candleKeeper.getLast()), timeout * 6, timeout, unit);
        //predictorService.scheduleWithFixedDelay(() -> predictor.nextCandle(candleKeeper.getElements()), timeout * 8, timeout, unit);
    }
}