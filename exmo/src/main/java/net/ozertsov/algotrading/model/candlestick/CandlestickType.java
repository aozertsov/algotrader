package net.ozertsov.algotrading.model.candlestick;

public enum CandlestickType {
    LONG,
    SHORT,
    FIXED,
    EMPTY
}
