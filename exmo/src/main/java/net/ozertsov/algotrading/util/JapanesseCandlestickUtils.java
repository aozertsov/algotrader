package net.ozertsov.algotrading.util;

import net.ozertsov.algotrading.model.candlestick.CandlestickType;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;

import java.util.Collection;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class JapanesseCandlestickUtils {

    public static boolean isLower(JapanesseCandlestick left, JapanesseCandlestick right) {
        double minLeft = left.getType() == CandlestickType.LONG ? left.getOpenPrice() : left.getClosePrice();
        double minRight = right.getType() == CandlestickType.LONG ? right.getOpenPrice() : right.getClosePrice();
        return minLeft < minRight;
    }

    public static boolean isBigger(JapanesseCandlestick left, JapanesseCandlestick right) {
        double minLeft = left.getType() == CandlestickType.LONG ? left.getOpenPrice() : left.getClosePrice();
        double minRight = right.getType() == CandlestickType.LONG ? right.getOpenPrice() : right.getClosePrice();
        return minLeft > minRight;
    }

    public static JapanesseCandlestick findMin(Collection<JapanesseCandlestick> candles) {
        return candles.stream().min((a, b) -> Double.compare(min(a.getOpenPrice(), a.getClosePrice()), min(b.getOpenPrice(), b.getClosePrice()))).get();
    }


    public static JapanesseCandlestick findMax(Collection<JapanesseCandlestick> candles) {
        return candles.stream().max((a, b) -> Double.compare(max(a.getOpenPrice(), a.getClosePrice()), max(b.getOpenPrice(), b.getClosePrice()))).get();
    }
}
