package net.ozertsov.algotrading.io;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collection;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Log4j2
public class HistoryWriter<T extends Recordable> {

    File file;
    boolean isAddSchema = false;

    public HistoryWriter(File file) {
        this.file = file;
    }

    public static <T extends Recordable> HistoryWriter<T> createDocument(File file){
        if (file.exists()) {
            log.info("file " + file.getAbsolutePath() + " exists");
            file.delete();
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new HistoryWriter<T>(file);
    }

    public static HistoryWriter createDocument(String dir, String fileName) {
        File file = Paths.get(dir, fileName + ".csv").toFile();
        return createDocument(file);
    }
    
    public void appendData(T element) {
//        System.out.println("try to write");
        String data = element.getData();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))){
            if (!isAddSchema) {
                writer.append(element.getSchema().toString());
                isAddSchema = true;
            }
            log.trace("element added");
            writer.append("\n");
            writer.append(data);
//            System.out.println("writed");
        } catch (IOException e) {
            System.out.println("error");
            e.printStackTrace();
        }
    }

    public void appendData(Collection<T> datas) {
        //        System.out.println("try to write");
        log.info("try to append collection to file: " + file.getAbsolutePath());
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))){
            for (T element : datas) {
                if (!isAddSchema) {
                    writer.append(element.getSchema().toString());
                    isAddSchema = true;
                }
                String dataString = element.getData();
                writer.append("\n");
                writer.append(dataString);
                log.trace("element added");
            }
//            System.out.println("writed");
        } catch (IOException e) {
            System.out.println("Error");
            e.printStackTrace();
        }
    }
}