package net.ozertsov.algotrading.predict.evaluate;

import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;

import java.util.List;

@Log4j2
abstract public class PredictEvaluator {
    public abstract JapanesseCandlestick predictCandle(List<JapanesseCandlestick> candles) throws EvaluationException;
}
