package net.ozertsov.algotrading.predictor;

import com.google.common.io.Resources;
import net.ozertsov.algotrading.io.HistoryReader;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import net.ozertsov.algotrading.predict.Predictor;
import net.ozertsov.algotrading.predict.writable.movingarrange.LongHorizontPolynomialMovingArangePredictor;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

@Ignore
public class LongHorizontPolynomialMovingArangePredictorTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder(new File("/media/alexander/Новый том/Repositore/Java/Algotrading"));

    @Test
    public void test1() throws URISyntaxException {
        URL uri = Resources.getResource("crowlerTestDataSet.csv");
        HistoryReader reader = new HistoryReader(new File(uri.toURI()));
        List<JapanesseCandlestick> candlestickList = reader.readHistory();


        Predictor predictor = new LongHorizontPolynomialMovingArangePredictor(folder.getRoot().toString(), 5, new double[]{0.45, 0.3, 0.1, 0.1, 0.05}, 3);
        System.out.println(predictor.nextCandle(candlestickList.subList(0, 5)));
    }

    @Test
    public void test2() throws URISyntaxException {
        URL uri = Resources.getResource("crowlerTestDataSet.csv");
        HistoryReader reader = new HistoryReader(new File(uri.toURI()));
        List<JapanesseCandlestick> candlestickList = reader.readHistory();

        Predictor predictor = new LongHorizontPolynomialMovingArangePredictor(folder.getRoot().toString(), 5, new double[]{0.45, 0.3, 0.1, 0.1, 0.05}, 3);
        System.out.println(predictor.nextCandle(candlestickList.subList(2, 7)));
    }
}
