package net.ozertsov.algotrading.crowl;

import lombok.extern.log4j.Log4j2;
import net.ozertsov.algotrading.converter.CandlestickConverter;
import net.ozertsov.algotrading.model.HistoryKeeper;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Trades;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.knowm.xchange.service.trade.TradeService;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

@Log4j2
public abstract class AbstractCrowler<TMarketDataService extends MarketDataService,
        TTradeService extends TradeService, TTrades extends Trades> {
    final CurrencyPair tradingPair;
    TMarketDataService marketDataService;
    TTradeService tradingService;
    CandlestickConverter<LimitOrder, TTrades> converter = new CandlestickConverter<>();
    Instant lastTime;

    HistoryKeeper keeper;

    public AbstractCrowler(CurrencyPair tradingPair, Class exchangeClass, HistoryKeeper keeper) {
        this.tradingPair = tradingPair;
        this.keeper = keeper;
        ExchangeSpecification specification = new ExchangeSpecification(exchangeClass);
        Exchange ex = ExchangeFactory.INSTANCE.createExchange(specification);

        marketDataService = (TMarketDataService) ex.getMarketDataService();
        tradingService = (TTradeService) ex.getTradeService();
        lastTime = Instant.now();
    }

    public void updateHistory() {
        TTrades trades = null;
        Instant toTime = Instant.now();
        try {
            log.info("try to build japenesse candlestick");
            trades = (TTrades) marketDataService.getTrades(tradingPair);
            OrderBook book = marketDataService.getOrderBook(tradingPair);
            List<LimitOrder> orders = book.getAsks();
            orders.addAll(book.getBids());
            JapanesseCandlestick candle = converter.convert(orders, trades, lastTime.getEpochSecond(), toTime.getEpochSecond());
            keeper.addElement(candle);
            lastTime = toTime;
        } catch (IOException e) {
            log.error(e);
        } catch (Exception e) {
            log.error(e);
            }
    }
}
