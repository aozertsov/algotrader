package net.ozertsov.algotrading.model.level;

import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
public class Chunk {
    final double from;
    final double to;
    final LevelType type;

    @Getter
    @Setter
    double value;

    @Getter
    @Setter
    int count;

    public Chunk(double from, double to, LevelType type) {
        this.from = from;
        this.to = to;
        this.type = type;
    }

    public boolean isMine(double value) {
        return value <= to && value >= from;
    }

    public void addValue(double value) {
        count++;
        if (type == LevelType.SUPPORT  && (this.value == 0.0 || value < this.value))
            this.value = value;
        if (type == LevelType.RESISTANCE  && (this.value == 0.0 || value > this.value))
            this.value = value;
    }
}
