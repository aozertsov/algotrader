package net.ozertsov.algotrading.io;

import com.fasterxml.jackson.core.FormatSchema;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class Schema implements FormatSchema {
    @Getter
    private List<Field> fields;

    @Override
    public String getSchemaType() {
        return "aozertsov";
    }

    public enum Type {
        OBJECT, INSTANT, ENUM, DOUBLE;
        private String name;
        private Type() { this.name = this.name().toLowerCase(); }
        public String getName() { return name; }
    };


    public static class Field {
        private final String name;    // name of the field.
        private final Type type;


        public Field(String name, Type type) {
            super();
            this.name = name;
            this.type = type;
        }
    }

    @Override
    public String toString() {
        return fields.stream().map(x -> x.name).collect(Collectors.joining(","));
    }
}
