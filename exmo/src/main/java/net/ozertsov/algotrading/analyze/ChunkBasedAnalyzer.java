package net.ozertsov.algotrading.analyze;

import net.ozertsov.algotrading.model.candlestick.CandlestickType;
import net.ozertsov.algotrading.model.candlestick.JapanesseCandlestick;
import net.ozertsov.algotrading.model.level.Chunk;
import net.ozertsov.algotrading.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.ozertsov.algotrading.model.level.LevelType.RESISTANCE;
import static net.ozertsov.algotrading.model.level.LevelType.SUPPORT;
import static net.ozertsov.algotrading.util.JapanesseCandlestickUtils.isBigger;
import static net.ozertsov.algotrading.util.JapanesseCandlestickUtils.isLower;

public class ChunkBasedAnalyzer implements Analyzer {
    public Object getSupportResistance(List<JapanesseCandlestick> marketData, long from, long to) {
        List<JapanesseCandlestick> graph = marketData.stream()
                .filter(x -> x.getType() != CandlestickType.EMPTY)
                .filter(x -> x.getFrom().getEpochSecond() >= from)
                .filter(x -> x.getTo().getEpochSecond() <= to)
                .collect(Collectors.toList());

        List<JapanesseCandlestick> localMins = new ArrayList<>();
        List<JapanesseCandlestick> localMaxs = new ArrayList<>();
        for (int i = 1; i < graph.size() - 1; i++) {
            if (isLower(graph.get(i), graph.get(i - 1)) && isLower(graph.get(i), graph.get(i + 1))) {
                localMins.add(graph.get(i));
            }

            if (isBigger(graph.get(i), graph.get(i - 1)) && isBigger(graph.get(i), graph.get(i + 1))) {
                localMaxs.add(graph.get(i));
            }
        }


        Optional<JapanesseCandlestick> absoluteShortMin = localMins.stream()
                .reduce((x, y) -> x.getClosePrice() < y.getClosePrice() ? x : y);

        Optional<JapanesseCandlestick> absoluteShortMax = localMins.stream()
                .reduce((x, y) -> x.getClosePrice() > y.getClosePrice() ? x : y);

        double chunkSize = (absoluteShortMax.get().getClosePrice() - absoluteShortMin.get().getClosePrice()) * 0.05;

        List<Chunk> chunks = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            chunks.add(new Chunk(absoluteShortMin.get().getClosePrice() +  chunkSize * (i - 1),
                    absoluteShortMin.get().getClosePrice() + chunkSize * i, SUPPORT));
        }

        double supportLine = chunks.stream()
                .reduce((x, y) -> x.getCount() > y.getCount() ? x : y)
                .get().getValue();

        System.out.println(supportLine);

        Optional<JapanesseCandlestick> absoluteLongMin = localMaxs.stream()
                .reduce((x, y) -> x.getOpenPrice() < y.getOpenPrice() ? x : y);
        Optional<JapanesseCandlestick> absoluteLongMax = localMaxs.stream()
                .reduce((x, y) -> x.getOpenPrice() > y.getOpenPrice() ? x : y);

        chunkSize = (absoluteLongMax.get().getOpenPrice() - absoluteLongMin.get().getOpenPrice()) * 0.05;

        List<Chunk> longChunks = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            longChunks.add(new Chunk(absoluteLongMin.get().getOpenPrice() +  chunkSize * (i - 1),
                    absoluteLongMin.get().getOpenPrice() + chunkSize * i, RESISTANCE));
        }

        System.out.println(longChunks);
        localMaxs.stream()
                .forEach(x -> longChunks.stream()
                        .filter(y -> y.isMine(x.getOpenPrice()))
                        .findFirst()
                        .get().addValue(x.getOpenPrice()));

        double resistanceLine = longChunks.stream()
                .reduce((x, y) -> x.getCount() > y.getCount() ? x : y)
                .get().getValue();

        return new Pair(supportLine, resistanceLine);
    }

    @Override
    public double getSupportPrice(Collection<JapanesseCandlestick> candles) {
        List<JapanesseCandlestick> graph = candles.stream()
                .filter(x -> x.getType() != CandlestickType.EMPTY)
                .collect(Collectors.toList());

        List<JapanesseCandlestick> localMins = new ArrayList<>();
        List<JapanesseCandlestick> localMaxs = new ArrayList<>();
        for (int i = 1; i < graph.size() - 1; i++) {
            if (isLower(graph.get(i), graph.get(i - 1)) && isLower(graph.get(i), graph.get(i + 1))) {
                localMins.add(graph.get(i));
            }

            if (isBigger(graph.get(i), graph.get(i - 1)) && isBigger(graph.get(i), graph.get(i + 1))) {
                localMaxs.add(graph.get(i));
            }
        }

        Optional<JapanesseCandlestick> absoluteShortMin = localMins.stream()
                .reduce((x, y) -> x.getClosePrice() < y.getClosePrice() ? x : y);
        Optional<JapanesseCandlestick> absoluteShortMax = localMins.stream()
                .reduce((x, y) -> x.getClosePrice() > y.getClosePrice() ? x : y);

        double chunkSize = (absoluteShortMax.get().getClosePrice() - absoluteShortMin.get().getClosePrice()) * 0.05;

        List<Chunk> chunks = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            chunks.add(new Chunk(absoluteShortMin.get().getClosePrice() +  chunkSize * (i - 1),
                    absoluteShortMin.get().getClosePrice() + chunkSize * i, SUPPORT));
        }

        localMins.stream()
                .forEach(x -> chunks.stream()
                        .filter(y -> y.isMine(x.getClosePrice()))
                        .findFirst()
                        .get().addValue(x.getClosePrice()));


        double supportLine = chunks.stream()
                .reduce((x, y) -> x.getCount() > y.getCount() ? x : y)
                .get().getValue();

        return supportLine;
    }

    @Override
    public double getResistancePrice(Collection<JapanesseCandlestick> candles) {
        List<JapanesseCandlestick> graph = candles.stream()
                .filter(x -> x.getType() != CandlestickType.EMPTY)
                .collect(Collectors.toList());

        List<JapanesseCandlestick> localMins = new ArrayList<>();
        List<JapanesseCandlestick> localMaxs = new ArrayList<>();
        for (int i = 1; i < graph.size() - 1; i++) {
            if (isLower(graph.get(i), graph.get(i - 1)) && isLower(graph.get(i), graph.get(i + 1))) {
                localMins.add(graph.get(i));
            }

            if (isBigger(graph.get(i), graph.get(i - 1)) && isBigger(graph.get(i), graph.get(i + 1))) {
                localMaxs.add(graph.get(i));
            }
        }

//        System.out.println(localMins);

        Optional<JapanesseCandlestick> absoluteShortMin = localMins.stream()
                .reduce((x, y) -> x.getClosePrice() < y.getClosePrice() ? x : y);
        Optional<JapanesseCandlestick> absoluteShortMax = localMins.stream()
                .reduce((x, y) -> x.getClosePrice() > y.getClosePrice() ? x : y);

        double chunkSize = (absoluteShortMax.get().getClosePrice() - absoluteShortMin.get().getClosePrice()) * 0.05;

        List<Chunk> chunks = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            chunks.add(new Chunk(absoluteShortMin.get().getClosePrice() +  chunkSize * (i - 1),
                    absoluteShortMin.get().getClosePrice() + chunkSize * i, SUPPORT));
        }

//        System.out.println(chunks);
        localMins.stream()
                .forEach(x -> chunks.stream()
                        .filter(y -> y.isMine(x.getClosePrice()))
                        .findFirst()
                        .get().addValue(x.getClosePrice()));


        Optional<JapanesseCandlestick> absoluteLongMin = localMaxs.stream()
                .reduce((x, y) -> x.getOpenPrice() < y.getOpenPrice() ? x : y);
        Optional<JapanesseCandlestick> absoluteLongMax = localMaxs.stream()
                .reduce((x, y) -> x.getOpenPrice() > y.getOpenPrice() ? x : y);

        chunkSize = (absoluteLongMax.get().getOpenPrice() - absoluteLongMin.get().getOpenPrice()) * 0.05;

        List<Chunk> longChunks = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            longChunks.add(new Chunk(absoluteLongMin.get().getOpenPrice() +  chunkSize * (i - 1),
                    absoluteLongMin.get().getOpenPrice() + chunkSize * i, RESISTANCE));
        }

        localMaxs.stream()
                .forEach(x -> longChunks.stream()
                        .filter(y -> y.isMine(x.getOpenPrice()))
                        .findFirst()
                        .get().addValue(x.getOpenPrice()));

        double resistanceLine = longChunks.stream()
                .reduce((x, y) -> x.getCount() > y.getCount() ? x : y)
                .get().getValue();

        return resistanceLine;
    }
}
